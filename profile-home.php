<?php include'header.php' ?>

<!-- sticky header end --> 
<!-- top sec start -->

<div class="container">
  <div class="row"> 

    <!-- banner outer start -->
    <div  class="col-sm-16 banner-outer">
      <div class="row">
        <div class="col-sm-16 col-md-16 col-lg-16"> 
          <div class="row">
           <div class="col-sm-16 col-md-16 col-lg-8"> 
            <div class="topic">
              <div class="carousel-caption" style="color: #fff">
                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                <div class="text-danger-white sub-info-bordered">
                  <div><a href="">Dec 9 2014 | </a></div>
                  <div><a href="">Entah Siapa </a></div>
                </div>
              </div>
              <a href=""> <img class="img-thumbnail" src="images/banner-slider/slide-img-1.jpg" width="1600" height="972" alt="" style="height: 406px;" /></a>

            </div>
          </div>
          
          <div class="col-sm-16 col-md-16 col-lg-4"> 
            <div class="topic">
              <div class="carousel-caption">
                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                <div class="text-danger-white sub-info-bordered">
                  <div><a href="">Dec 9 2014 |</a></div>
                  <div><a href="">Entah Siapa</a></div>
                </div>
              </div>
              <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

            </div>
          </div>

          <div class="col-sm-16 col-md-16 col-lg-4"> 
            <div class="topic">
              <div class="carousel-caption">
                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                <div class="text-danger-white sub-info-bordered">
                  <div><a href="">Dec 9 2014 |</a></div>
                  <div><a href="">Entah Siapa</a></div>
                </div>
              </div>
              <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

            </div>
          </div>

          <div class="col-sm-16 col-md-16 col-lg-4"> 
            <div class="topic">
              <div class="carousel-caption">
                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                <div class="text-danger-white sub-info-bordered">
                  <div><a href="">Dec 9 2014 |</a></div>
                  <div><a href="">Entah Siapa</a></div>
                </div>
              </div>
              <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

            </div>
          </div>

          <div class="col-sm-16 col-md-16 col-lg-4"> 
            <div class="topic">
              <div class="carousel-caption">
                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                <div class="text-danger-white sub-info-bordered">
                  <a href="">Dec 9 2014 |</a>
                  <a href="">Entah Siapa</a>
                </div>
              </div>
              <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- banner outer end --> 

</div>
</div>
<!-- top sec end --> 

<!-- data start -->

<div class="container">
  <div class="row boxer"> 
    <!-- left sec start -->
    <div class="col-md-16 col-sm-16">
      <div class="row"> 

       <!-- news start -->
       <div class="col-md-16 col-sm-16">
        <div class="main-title-outer pull-left">
          <div class="main-title">News</div>
          <a href="">
            <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
          </div>
          <div class="row">
            <div class="col-md-4 col-sm-0 col-xs-16 hidden-x">

              <div class="box-menu">
                <a href="#">
                  <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                  <div style="z-index: 2;margin-top: -92px;">
                    <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                  </div>
                </a>
                <div class="caption" >                   
                  <h5 class="list-group-item-heading">POLITICS</h5>
                  <h5 class="list-group-item-heading">ECONOMICS</h5>
                  <h5 class="list-group-item-heading">CRIMINALS</h5>
                </div>       
              </div>
            </div>
            <div class="col-md-7 col-sm-10 col-xs-16">
              <ul class="list-unstyled">
                
                <li>
                  <div class="row">
                    
                    <div class="col-sm-16 col-md-16 col-lg-16 ">
                      <span class="minitext">
                        <a href="#">30 Sep 2016</a>|
                        <a href="#">Entah Siapa</a>
                      </span>
                      <a href="#">
                        <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                      </div>
                    </div>
                  </li>

                  <li>
                    <div class="row"> 
                      <div class="col-sm-16 col-md-16 col-lg-16 ">
                        <span class="minitext">
                          <a href="#">30 Sep 2016</a>|
                          <a href="#">Entah Siapa</a>
                        </span>
                        <a href="#">
                          <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="row"> 
                        <div class="col-sm-16 col-md-16 col-lg-16 ">
                          <span class="minitext">
                            <a href="#">30 Sep 2016</a>|
                            <a href="#">Entah Siapa</a>
                          </span>
                          <a href="#">
                            <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="row"> 
                          <div class="col-sm-16 col-md-16 col-lg-16 ">
                            <span class="minitext">
                              <a href="#">30 Sep 2016</a>|
                              <a href="#">Entah Siapa</a>
                            </span>
                            <a href="#">
                              <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                            </div>
                          </div>
                        </li>

                      </ul>
                    </div>

                    <div class="col-md-5 col-sm-6 col-xs-16">
                      <div class="topic" style="padding-bottom: 10px;">
                        <div class="carousel-caption">
                          <a href="">Best snack ever: mini mac and cheese cupcakes</a>
                          <div class="text-danger sub-info-bordered">
                            <a href="">Dec 9 2014 | </a>
                            <a href="">Entah Siapa </a>
                          </div>
                        </div>
                        <a href=""><img class="img-thumbnail" src="http://placehold.it/400x440" width="1600" height="972" alt=""/></a></div> 
                      </div>

                    </div>
                    <hr>
                  </div>

                  
                  <!-- news end --> 

                  <!-- hunian start -->
                  
                  <div class="col-md-16 col-sm-16">
                    <div class="main-title-outer pull-left">
                      <div class="main-title">Hunian</div>
                      <a href="">
                        <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                      </div>
                      <div class="row">
                        <div class="col-md-4 col-sm-0 col-xs-16">

                          <div class="box-menu">
                            <a href="#">
                              <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                              <div style="z-index: 2;margin-top: -92px;">
                                <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                              </div>
                            </a>
                            <div class="caption">                   
                              <h5 class="list-group-item-heading">JAKARTA PROPERTY</h5>
                              <h5 class="list-group-item-heading">HOME LIFESTYLE</h5>
                              <h5 class="list-group-item-heading">KPR</h5>
                            </div>       
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-8 col-xs-16">
                          <div class="topic">
                            <div class="carousel-caption">
                              <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                              <div class="text-danger-white sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                            <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                            
                          </div>
                        </div>
                      </div>
                      <hr>
                    </div>
                    <!-- hunian end --> 

                  </div>
                </div>
              </div>
            </div>

            <!-- profile home -->
            <div class="profile-home" style="background-color: #e6e1e1;
            padding-top: 30px;padding-bottom: 20px;">
            <div class="container">
              <div class="row boxer"> 
                <!-- left sec start -->
                <div class="col-md-16 col-sm-16">
                  <div class="row"> 

                    <!-- profile start -->
                    <div class="col-md-16 col-sm-16">
                      <div class="main-title-outer pull-left">
                        <div class="main-title">Dr. Azkan D. Saputra</div>

                      </div>
                      <div class="row">
                        <div class="col-md-4 col-sm-5x col-xs-16">
                          <div class="topic box-menu-profile"> <a href="#"><img class="img-thumbnail" src="images/profile.jpg" width="600" height="398" alt="" /></a>
                           <div class="bar-tag">
                             <a type="button" class="btn btn-default btn-sm btn-ads-profile">
                              PROFILE</a>
                            </div> 
                            <div class="caption">  
                              <a href="#"><h3>Spesialis Kesehatan</h3></a>
                              <div class="text-danger sub-info-bordered">
                                <p>Kulit Yang Sehat adalah Kulit yang Bersih</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-5x col-xs-16">
                          <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                            <div class="captionx">  
                              <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                              <div class="text-danger sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-4 col-sm-5x col-xs-16">
                          <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                            <div class="captionx">  
                              <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                              <div class="text-danger sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-5x col-xs-16">
                          <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                            <div class="captionx">  
                              <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                              <div class="text-danger sub-info-bordered">
                                <a href="#">Dec 9 2014 |</a>
                                <a href="#">Entah Siapa</a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                    <!-- profile end --> 

                  </div>
                </div>
                <!--  -->
              </div>
            </div>
          </div>
          <!-- end profile home -->


          <div class="container">
            <div class="row boxer"> 
              <!-- left sec start -->
              <div class="col-md-16 col-sm-16">
                <div class="row">
                 <!-- lifestyle start -->
                 <div class="col-md-16 col-sm-16">
                  <div class="main-title-outer pull-left">
                    <div class="main-title">Lifestyle</div>
                    <a href="">
                      <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                    </div>
                    <div class="row">
                      <div class="col-md-4 col-sm-0 col-xs-16">

                        <div class="box-menu">
                          <a href="#">
                            <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                            <div style="z-index: 2;margin-top: -92px;">
                              <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                            </div>
                          </a>
                          <div class="caption" >                   
                            <h5 class="list-group-item-heading">POLITICS</h5>
                            <h5 class="list-group-item-heading">ECONOMICS</h5>
                            <h5 class="list-group-item-heading">CRIMINALS</h5>
                          </div>       
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-5x col-xs-16">
                        <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                          <div class="captionx">  
                            <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                            <div class="text-danger sub-info-bordered">
                              <a href="#">Dec 9 2014 |</a>
                              <a href="#">Entah Siapa</a>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-5x col-xs-16">
                        <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                          <div class="captionx">  
                            <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                            <div class="text-danger sub-info-bordered">
                              <a href="#">Dec 9 2014 |</a>
                              <a href="#">Entah Siapa</a>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-4 col-sm-5x col-xs-16">
                        <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                          <div class="captionx">  
                            <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                            <div class="text-danger sub-info-bordered">
                              <a href="#">Dec 9 2014 |</a>
                              <a href="#">Entah Siapa</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr>
                  </div>
                  <!-- lifestyle end -->  


          <!-- tech start -->
          <div class="col-md-16 col-sm-16">
            <div class="main-title-outer pull-left">
              <div class="main-title">Tech</div>
              <a href="">
                <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
              </div>
              <div class="row">
                <div class="col-md-4 col-sm-0 col-xs-16">

                  <div class="box-menu">
                    <a href="#">
                      <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                      <div style="z-index: 2;margin-top: -92px;">
                        <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                      </div>
                    </a>
                    <div class="caption">                   
                      <h5 class="list-group-item-heading">POLITICS</h5>
                      <h5 class="list-group-item-heading">ECONOMICS</h5>
                      <h5 class="list-group-item-heading">CRIMINALS</h5>
                    </div>       
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

                <div class="col-md-4 col-sm-8 col-xs-16">
                  <div class="topic">
                    <div class="carousel-caption">
                      <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                      <div class="text-danger-white sub-info-bordered">
                        <a href="#">Dec 9 2014 |</a>
                        <a href="#">Entah Siapa</a>
                      </div>
                    </div>
                    <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>
                    
                  </div>
                </div>

              </div>
              <hr>
            </div>
            <!-- tech end --> 

            <!-- entertainment start -->
            <div class="col-md-16 col-sm-16">
              <div class="main-title-outer pull-left">
                <div class="main-title">Entertainment</div>
                <a href="">
                  <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                </div>
                <div class="row">
                  <div class="col-md-4 col-sm-0 col-xs-16">

                    <div class="box-menu">
                      <a href="#">
                        <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                        <div style="z-index: 2;margin-top: -92px;">
                          <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                        </div>
                      </a>
                      <div class="caption" >                   
                        <h5 class="list-group-item-heading">POLITICS</h5>
                        <h5 class="list-group-item-heading">ECONOMICS</h5>
                        <h5 class="list-group-item-heading">CRIMINALS</h5>
                      </div>       
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-5x col-xs-16">
                    <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                      <div class="captionx">  
                        <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                        <div class="text-danger sub-info-bordered">
                          <a href="#">Dec 9 2014 |</a>
                          <a href="#">Entah Siapa</a>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4 col-sm-5x col-xs-16">
                    <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                      <div class="captionx">  
                        <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                        <div class="text-danger sub-info-bordered">
                          <a href="#">Dec 9 2014 |</a>
                          <a href="#">Entah Siapa</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <div class="col-md-4 col-sm-5x col-xs-16">
                    <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                      <div class="captionx">  
                        <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                        <div class="text-danger sub-info-bordered">
                          <a href="#">Dec 9 2014 |</a>
                          <a href="#">Entah Siapa</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr>
              </div>
              <!-- entertainment end --> 

              <!-- community start -->
              <div class="col-md-16 col-sm-16">
                <div class="main-title-outer pull-left">
                  <div class="main-title">Community</div>
                  <a href="">
                    <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                  </div>
                  <div class="row">
                    <div class="col-md-4 col-sm-0 col-xs-16 hidden-x">

                      <div class="box-menu">
                        <a href="#">
                          <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                          <div style="z-index: 2;margin-top: -92px;">
                            <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                          </div>
                        </a>
                        <div class="caption" >                   
                          <h5 class="list-group-item-heading">POLITICS</h5>
                          <h5 class="list-group-item-heading">ECONOMICS</h5>
                          <h5 class="list-group-item-heading">CRIMINALS</h5>
                        </div>       
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-10 col-xs-16">
                      <ul class="list-unstyled">
                        
                        <li>
                          <div class="row">
                            
                            <div class="col-sm-16 col-md-16 col-lg-16 ">
                              <span class="minitext">
                                <a href="#">30 Sep 2016</a>|
                                <a href="#">Entah Siapa</a>
                              </span>
                              <a href="#">
                                <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                              </div>
                            </div>
                          </li>

                          <li>
                            <div class="row"> 
                              <div class="col-sm-16 col-md-16 col-lg-16 ">
                                <span class="minitext">
                                  <a href="#">30 Sep 2016</a>|
                                  <a href="#">Entah Siapa</a>
                                </span>
                                <a href="#">
                                  <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                </div>
                              </div>
                            </li>

                            <li>
                              <div class="row"> 
                                <div class="col-sm-16 col-md-16 col-lg-16 ">
                                  <span class="minitext">
                                    <a href="#">30 Sep 2016</a>|
                                    <a href="#">Entah Siapa</a>
                                  </span>
                                  <a href="#">
                                    <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                  </div>
                                </div>
                              </li>

                              <li>
                                <div class="row"> 
                                  <div class="col-sm-16 col-md-16 col-lg-16 ">
                                    <span class="minitext">
                                      <a href="#">30 Sep 2016</a>|
                                      <a href="#">Entah Siapa</a>
                                    </span>
                                    <a href="#">
                                      <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                    </div>
                                  </div>
                                </li>

                              </ul>
                            </div>

                            <div class="col-md-5 col-sm-6 col-xs-16">
                              <div class="topic" style="padding-bottom: 10px;">
                                <div class="carousel-caption">
                                  <a href="">Best snack ever: mini mac and cheese cupcakes</a>
                                  <div class="text-danger sub-info-bordered">
                                    <a href="">Dec 9 2014 | </a>
                                    <a href="">Entah Siapa </a>
                                  </div>
                                </div>
                                <a href=""><img class="img-responsive" src="http://placehold.it/400x440" width="1600" height="972" alt=""/></a>
                                </div>
                              </div>

                            </div>
                            <hr>
                          </div>
                          <!-- community end --> 
                          <div class="col-lg-16">
                           <div class="row">
                            <div class="col-md-16 col-sm-16 col-xs-16">
                              <div class="row">
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                  <!-- Indicators -->
                                  <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                  </ol>

                                  <!-- Wrapper for slides -->
                                  <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                      <img src="images/iklan/iklan1.jpg" class="img-responsive">
                                    </div>
                                    <div class="item">
                                      <img src="images/iklan/iklan2.jpg" class="img-responsive">
                                    </div>
                                    <div class="item">
                                      <img src="images/iklan/iklan3.jpg" class="img-responsive">
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div><!-- /.container -->

                      <!--Recent videos end--> 
                      <!--wide ad start-->

                    </div>
                  </div>
                  <!-- left sec end --> 
                </div>
              </div>
              <!-- data end --> 


              <?php include'footer.php' ?>



