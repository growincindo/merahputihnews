<div id="page-content">
  <!--- home menu -->
  <div class="container dxd" >
    <div class="row"> 

      <!-- banner outer start -->
      <div  class="col-sm-16 banner-outer">
        <div class="row">
          <div class="col-sm-16 col-md-16 col-lg-16"> 
            <div class="row">
             <div class="col-sm-16 col-md-16 col-lg-8"> 
              <div class="topic">
                <div class="carousel-caption" style="color: #fff">
                  <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                  <div class="text-danger-white sub-info-bordered">
                    <div><a href="">Dec 9 2014 | </a></div>
                    <div><a href="">Entah Siapa </a></div>
                  </div>
                </div>
                <a href=""> <img class="img-thumbnail" src="images/banner-slider/slide-img-1.jpg" width="1600" height="972" alt="" style="height: 406px;" /></a>

              </div>
            </div>

            <div class="col-sm-16 col-md-16 col-lg-4"> 
              <div class="topic">
                <div class="carousel-caption">
                  <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                  <div class="text-danger-white sub-info-bordered">
                    <div><a href="">Dec 9 2014 |</a></div>
                    <div><a href="">Entah Siapa</a></div>
                  </div>
                </div>
                <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

              </div>
            </div>

            <div class="col-sm-16 col-md-16 col-lg-4"> 
              <div class="topic">
                <div class="carousel-caption">
                  <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                  <div class="text-danger-white sub-info-bordered">
                    <div><a href="">Dec 9 2014 |</a></div>
                    <div><a href="">Entah Siapa</a></div>
                  </div>
                </div>
                <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

              </div>
            </div>

            <div class="col-sm-16 col-md-16 col-lg-4"> 
              <div class="topic">
                <div class="carousel-caption">
                  <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                  <div class="text-danger-white sub-info-bordered">
                    <div><a href="">Dec 9 2014 |</a></div>
                    <div><a href="">Entah Siapa</a></div>
                  </div>
                </div>
                <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

              </div>
            </div>

            <div class="col-sm-16 col-md-16 col-lg-4"> 
              <div class="topic">
                <div class="carousel-caption">
                  <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                  <div class="text-danger-white sub-info-bordered">
                    <a href="">Dec 9 2014 |</a>
                    <a href="">Entah Siapa</a>
                  </div>
                </div>
                <a href=""><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
    <!-- banner outer end --> 

  </div>
</div>
<!-- top sec slide mobile --> 
<div class="col-lg-16 mobile-show">
  <div class="row">       
<!-- Carousel
  ================================================== -->
  <div id="owl-demo" class="owl-carousel" style="background-color: #d21112;">
    <div class="owl-nopading">
      <img src="http://placehold.it/1170x800/101010/FFFFFF">
      <div class="carousel-caption">
        <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
        <div class="text-danger-white sub-info-bordered">
          <a href="">Dec 9 2014 |</a>
          <a href="">Entah Siapa</a>
        </div>
      </div>
    </div>
    <div class="owl-nopading">
      <img src="http://placehold.it/1170x800/42bdc2/FFFFFF">
      <div class="carousel-caption">
        <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
        <div class="text-danger-white sub-info-bordered">
          <a href="">Dec 9 2014 |</a>
          <a href="">Entah Siapa</a>
        </div>
      </div>
    </div>
    <div class="owl-nopading">
      <img src="http://placehold.it/1170x800/bfbfbf/FFFFFF">
      <div class="carousel-caption">
        <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
        <div class="text-danger-white sub-info-bordered">
          <a href="">Dec 9 2014 |</a>
          <a href="">Entah Siapa</a>
        </div>
      </div>
    </div>
  </div>

</div>
</div>
</div>
<!-- /.container -->


<!-- data start -->

<div class="container">
  <div class="row boxer"> 
    <!-- left sec start -->
    <div class="col-md-16 col-sm-16">
      <div class="row"> 

        <!-- news start -->
        <div class="col-md-16 col-sm-16">
          <div class="main-title-outer pull-left">
            <div class="main-title">News</div>
            <a href="">
              <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-0 col-xs-16 hidden-x">

                <div class="box-menu">
                  <a href="#">
                    <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                    <div style="z-index: 2;margin-top: -92px;">
                      <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                    </div>
                  </a>
                  <div class="caption" >                   
                    <h5 class="list-group-item-heading">POLITICS</h5>
                    <h5 class="list-group-item-heading">ECONOMICS</h5>
                    <h5 class="list-group-item-heading">CRIMINALS</h5>
                  </div>       
                </div>
              </div>
              <div class="col-md-7 col-sm-10 col-xs-16">
                <ul class="list-unstyled">

                  <li>
                    <div class="row">

                      <div class="col-sm-16 col-md-16 col-lg-16 ">
                        <span class="minitext">
                          <a href="#">30 Sep 2016</a>|
                          <a href="#">Entah Siapa</a>
                        </span>
                        <a href="#">
                          <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                        </div>
                      </div>
                    </li>

                    <li>
                      <div class="row"> 
                        <div class="col-sm-16 col-md-16 col-lg-16 ">
                          <span class="minitext">
                            <a href="#">30 Sep 2016</a>|
                            <a href="#">Entah Siapa</a>
                          </span>
                          <a href="#">
                            <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                          </div>
                        </div>
                      </li>

                      <li>
                        <div class="row"> 
                          <div class="col-sm-16 col-md-16 col-lg-16 ">
                            <span class="minitext">
                              <a href="#">30 Sep 2016</a>|
                              <a href="#">Entah Siapa</a>
                            </span>
                            <a href="#">
                              <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                            </div>
                          </div>
                        </li>

                        <li>
                          <div class="row"> 
                            <div class="col-sm-16 col-md-16 col-lg-16 ">
                              <span class="minitext">
                                <a href="#">30 Sep 2016</a>|
                                <a href="#">Entah Siapa</a>
                              </span>
                              <a href="#">
                                <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                              </div>
                            </div>
                          </li>

                        </ul>
                      </div>

                      <div class="col-md-5 col-sm-6 col-xs-16">
                        <div class="topic" style="padding-bottom: 10px;">
                          <div class="carousel-caption">
                            <a href="">Best snack ever: mini mac and cheese cupcakes</a>
                            <div class="text-danger sub-info-bordered">
                              <a href="">Dec 9 2014 | </a>
                              <a href="">Entah Siapa </a>
                            </div>
                          </div>
                          <a href=""><img class="img-thumbnail" src="http://placehold.it/400x440" width="1600" height="972" alt=""/></a></div> 
                        </div>

                      </div>
                      <hr>
                    </div>


                    <!-- news end --> 

                    <!-- hunian start -->

                    <div class="col-md-16 col-sm-16">
                      <div class="main-title-outer pull-left">
                        <div class="main-title">Hunian</div>
                        <a href="">
                          <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                        </div>
                        <div class="row">
                          <div class="col-md-4 col-sm-0 col-xs-16">

                            <div class="box-menu">
                              <a href="#">
                                <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                                <div style="z-index: 2;margin-top: -92px;">
                                  <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                                </div>
                              </a>
                              <div class="caption">                   
                                <h5 class="list-group-item-heading">JAKARTA PROPERTY</h5>
                                <h5 class="list-group-item-heading">HOME LIFESTYLE</h5>
                                <h5 class="list-group-item-heading">KPR</h5>
                              </div>       
                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>

                          <div class="col-md-4 col-sm-8 col-xs-16">
                            <div class="topic">
                              <div class="carousel-caption">
                                <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                <div class="text-danger-white sub-info-bordered">
                                  <a href="#">Dec 9 2014 |</a>
                                  <a href="#">Entah Siapa</a>
                                </div>
                              </div>
                              <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                            </div>
                          </div>
                        </div>
                        <hr>
                      </div>
                      <!-- hunian end --> 

                      <!-- lifestyle start -->
                      <div class="col-md-16 col-sm-16">
                        <div class="main-title-outer pull-left">
                          <div class="main-title">Lifestyle</div>
                          <a href="">
                            <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                          </div>
                          <div class="row">
                            <div class="col-md-4 col-sm-0 col-xs-16">

                              <div class="box-menu">
                                <a href="#">
                                  <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                                  <div style="z-index: 2;margin-top: -92px;">
                                    <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                                  </div>
                                </a>
                                <div class="caption" >                   
                                  <h5 class="list-group-item-heading">POLITICS</h5>
                                  <h5 class="list-group-item-heading">ECONOMICS</h5>
                                  <h5 class="list-group-item-heading">CRIMINALS</h5>
                                </div>       
                              </div>
                            </div>

                            <div class="col-md-4 col-sm-5x col-xs-16">
                              <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                <div class="captionx">  
                                  <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                  <div class="text-danger sub-info-bordered">
                                    <a href="#">Dec 9 2014 |</a>
                                    <a href="#">Entah Siapa</a>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-4 col-sm-5x col-xs-16">
                              <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                <div class="captionx">  
                                  <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                  <div class="text-danger sub-info-bordered">
                                    <a href="#">Dec 9 2014 |</a>
                                    <a href="#">Entah Siapa</a>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-4 col-sm-5x col-xs-16">
                              <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                <div class="captionx">  
                                  <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                  <div class="text-danger sub-info-bordered">
                                    <a href="#">Dec 9 2014 |</a>
                                    <a href="#">Entah Siapa</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <hr>
                        </div>
                        <!-- lifestyle end --> 


                        <!-- tech start -->
                        <div class="col-md-16 col-sm-16">
                          <div class="main-title-outer pull-left">
                            <div class="main-title">Tech</div>
                            <a href="">
                              <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                            </div>
                            <div class="row">
                              <div class="col-md-4 col-sm-0 col-xs-16">

                                <div class="box-menu">
                                  <a href="#">
                                    <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                                    <div style="z-index: 2;margin-top: -92px;">
                                      <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                                    </div>
                                  </a>
                                  <div class="caption">                   
                                    <h5 class="list-group-item-heading">POLITICS</h5>
                                    <h5 class="list-group-item-heading">ECONOMICS</h5>
                                    <h5 class="list-group-item-heading">CRIMINALS</h5>
                                  </div>       
                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-3.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                              <div class="col-md-4 col-sm-8 col-xs-16">
                                <div class="topic">
                                  <div class="carousel-caption">
                                    <a href="#">Best snack ever: mini mac and cheese cupcakes  </a>
                                    <div class="text-danger-white sub-info-bordered">
                                      <a href="#">Dec 9 2014 |</a>
                                      <a href="#">Entah Siapa</a>
                                    </div>
                                  </div>
                                  <a href="#"><img class="img-thumbnail" src="images/banner-static/bs-2.jpg" width="1600" height="972" alt=""/></a>

                                </div>
                              </div>

                            </div>
                            <hr>
                          </div>
                          <!-- tech end --> 

                          <!-- entertainment start -->
                          <div class="col-md-16 col-sm-16">
                            <div class="main-title-outer pull-left">
                              <div class="main-title">Entertainment</div>
                              <a href="">
                                <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                              </div>
                              <div class="row">
                                <div class="col-md-4 col-sm-0 col-xs-16">

                                  <div class="box-menu">
                                    <a href="#">
                                      <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                                      <div style="z-index: 2;margin-top: -92px;">
                                        <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                                      </div>
                                    </a>
                                    <div class="caption" >                   
                                      <h5 class="list-group-item-heading">POLITICS</h5>
                                      <h5 class="list-group-item-heading">ECONOMICS</h5>
                                      <h5 class="list-group-item-heading">CRIMINALS</h5>
                                    </div>       
                                  </div>
                                </div>

                                <div class="col-md-4 col-sm-5x col-xs-16">
                                  <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                    <div class="captionx">  
                                      <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                      <div class="text-danger sub-info-bordered">
                                        <a href="#">Dec 9 2014 |</a>
                                        <a href="#">Entah Siapa</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-4 col-sm-5x col-xs-16">
                                  <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                    <div class="captionx">  
                                      <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                      <div class="text-danger sub-info-bordered">
                                        <a href="#">Dec 9 2014 |</a>
                                        <a href="#">Entah Siapa</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-4 col-sm-5x col-xs-16">
                                  <div class="topic"> <a href="#"><img class="img-thumbnail" src="images/news/news1.png" width="600" height="398" alt="" /></a>
                                    <div class="captionx">  
                                      <a href="#"><h3>Famous artist share his tracks for free</h3></a>
                                      <div class="text-danger sub-info-bordered">
                                        <a href="#">Dec 9 2014 |</a>
                                        <a href="#">Entah Siapa</a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <hr>
                            </div>
                            <!-- entertainment end --> 

                            <!-- community start -->
                            <div class="col-md-16 col-sm-16">
                              <div class="main-title-outer pull-left">
                                <div class="main-title">Community</div>
                                <a href="">
                                  <div class="span-outer"><span class="pull-right text-danger-lg last-update"><span class=""></span>view all +</span> </div></a>
                                </div>
                                <div class="row">
                                  <div class="col-md-4 col-sm-0 col-xs-16 hidden-x">

                                    <div class="box-menu">
                                      <a href="#">
                                        <img class="img-thumbnail" src="http://placehold.it/390x200" alt="">
                                        <div style="z-index: 2;margin-top: -92px;">
                                          <img class="img-responsive bulat" src="http://placehold.it/390x200" alt="">
                                        </div>
                                      </a>
                                      <div class="caption" >                   
                                        <h5 class="list-group-item-heading">POLITICS</h5>
                                        <h5 class="list-group-item-heading">ECONOMICS</h5>
                                        <h5 class="list-group-item-heading">CRIMINALS</h5>
                                      </div>       
                                    </div>
                                  </div>
                                  <div class="col-md-7 col-sm-10 col-xs-16">
                                    <ul class="list-unstyled">

                                      <li>
                                        <div class="row">

                                          <div class="col-sm-16 col-md-16 col-lg-16 ">
                                            <span class="minitext">
                                              <a href="#">30 Sep 2016</a>|
                                              <a href="#">Entah Siapa</a>
                                            </span>
                                            <a href="#">
                                              <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                            </div>
                                          </div>
                                        </li>

                                        <li>
                                          <div class="row"> 
                                            <div class="col-sm-16 col-md-16 col-lg-16 ">
                                              <span class="minitext">
                                                <a href="#">30 Sep 2016</a>|
                                                <a href="#">Entah Siapa</a>
                                              </span>
                                              <a href="#">
                                                <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                              </div>
                                            </div>
                                          </li>

                                          <li>
                                            <div class="row"> 
                                              <div class="col-sm-16 col-md-16 col-lg-16 ">
                                                <span class="minitext">
                                                  <a href="#">30 Sep 2016</a>|
                                                  <a href="#">Entah Siapa</a>
                                                </span>
                                                <a href="#">
                                                  <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                                </div>
                                              </div>
                                            </li>

                                            <li>
                                              <div class="row"> 
                                                <div class="col-sm-16 col-md-16 col-lg-16 ">
                                                  <span class="minitext">
                                                    <a href="#">30 Sep 2016</a>|
                                                    <a href="#">Entah Siapa</a>
                                                  </span>
                                                  <a href="#">
                                                    <h4 class="list-group-item-heading">Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit.</h4></a>
                                                  </div>
                                                </div>
                                              </li>

                                            </ul>
                                          </div>

                                          <div class="col-md-5 col-sm-6 col-xs-16">
                                            <div class="topic" style="padding-bottom: 10px;">
                                              <div class="carousel-caption">
                                                <a href="">Best snack ever: mini mac and cheese cupcakes</a>
                                                <div class="text-danger sub-info-bordered">
                                                  <a href="">Dec 9 2014 | </a>
                                                  <a href="">Entah Siapa </a>
                                                </div>
                                              </div>
                                              <a href=""><img class="img-responsive" src="http://placehold.it/400x440" width="1600" height="972" alt=""/></a>
                                            </div>
                                          </div>

                                        </div>
                                        <hr>
                                      </div>
                                      <!-- community end --> 

                                      <div class="col-md-16 col-sm-16 col-xs-16">
                                       <div class="row">
                                       <div class="col-md-16">
                                        <div id="owl-demo2" class="owl-carousel" style="background-color: #d21112;">
                                          <div class="owl-nopading">
                                            <img src="http://placehold.it/1170x600/101010/FFFFFF">
                                          </div>
                                          <div class="owl-nopading">
                                            <img src="http://placehold.it/1170x600/42bdc2/FFFFFF">
                                          </div>
                                          <div class="owl-nopading">
                                            <img src="http://placehold.it/1170x600/bfbfbf/FFFFFF">
                                          </div>
                                        </div>
                                      </div>
                                      </div>

                                    </div>



                                  </div><!-- /.container -->
                                </div>
                              </div>
                              <!-- left sec end --> 
                            </div>

                            <!-- data end --> 
                          </div>



                          <!-- Modal -->
                           <!--  <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div style="background-color: #2f2e2e;padding: 25px;border-radius: 5px;">
                                        <div class="modal-header" style="background-color: red;height: 185px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
                                            <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
                                            <span style="    
                                                z-index: 2;
                                                position: absolute;
                                                background-color: white;
                                                height: 27px;
                                                width: 27px;
                                                border-radius: 100px;
                                                padding: 0px;
                                                margin: -28px 0px 0px 0px;">
                                                &times;
                                                </span>
                                            </button>
                                            <div style="z-index: 1;"> -->
                                             <!-- <img src="img/mp-popup.png" style="margin-left: -15px;margin-top: -19px;"> -->
                                            <!--  </div>
                                             </div>
                                        <div class="modal-body" style="height: 320px;background-color: #f5f3f3;border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;">
                                            <center>
                                            <h1>SUBSCRIBE</h1>
                                            <h4 style="color: #828080;">Join our mailling list to recieve news <br>and update from our team</h4>
                                             <form class="form-inline" style="margin-top: 100px;">
                                                <div class="form-group">
                                                  <input type="email" class="form-control" id="email" placeholder="Enter email">
                                                </div>
                                                <button type="submit" class="btn btn-default" style="background-color: red;width: 100px;color: #fff">OK!</button>
                                              </form>    
                                            </center>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                              </div> -->
                              <!-- modal end -->

