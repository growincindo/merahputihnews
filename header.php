<!DOCTYPE html>
<html>
<!-- Mirrored from webyzona.com//templates/themeforest/globalnews/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 29 Jul 2015 14:00:55 GMT -->
<head style="overflow: hidden;">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Merahputih - Home</title>
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <!-- bootstrap styles-->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <!-- google font -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
  <!-- ionicons font -->
  <link href="css/ionicons.min.css" rel="stylesheet">
  <!-- animation styles -->
  <link rel="stylesheet" href="css/animate.css" />
  <!-- custom styles -->
  <link href="css/custom-red.css" rel="stylesheet" id="style">
  <!-- owl carousel styles-->
  
  <link rel="stylesheet" type="text/css" href="css/owl-carousel/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="css/owl-carousel/owl.theme.css">
  <link rel="stylesheet" type="text/css" href="css/owl-carousel/owl.transitions.css">
  <!-- magnific popup styles -->
  <link rel="stylesheet" href="css/magnific-popup.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


</head>

<body id="main">
<div class="jarak"></div>
<div class="wrapper"> 

  <!-- sticky header start -->
  <div class="sticky-header affix-header" 
  data-spy="affix-header" data-offset-top="0" id="main"> 
  <!-- header start -->

  <div class="nav-search-outer"> 
    <!-- nav start -->

    <!-- nvbar -->

    <!-- header end --> 
    <!-- nav and search start -->
    <div class="nav-search-outer"> 
      <!-- nav start -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">

          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" 
            aria-expanded="false" style=";cursor:pointer" onclick="openNav()">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="btn btn-xs mobile-show search-mini"  href="#search" aria-expanded="false">
            <span class="fa fa-search"></span></a>
            <a href="index.php"><img src="images/mp-main-logo.png"  class="red-bg-gradient" alt=""></a> 
          </div>

          <div class="navbar-inverse dxd">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav text-uppercase main-nav ">
                <li class="active"><a href="">News <span class="sr-only"></span></a></li>
                <li><a href="#">Hunian</a></li>
                <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">lifestyle <span class="caret"></span></a>
                  <ul class="dropdown-menu text-capitalize mega-menu" role="menu">
                    <li>
                      <div class="row">
                        <div class="col-sm-16">
                          <div class="row">
                            <div class="col-xs-16 col-sm-16 col-md-6 col-lg-6">
                              <ul class="mega-sub">
                                <li><a href="#">Home Decoration<span class="badge pull-right">Featured</span></a> </li>
                                <li><a href="#">Handbags &amp; Shoes</a> </li>
                                <li><a href="#">Furnishings &amp; Homeware<span class="badge pull-right">New</span></a> </li>
                                <li><a href="#">Beauty &amp; Fashion</a> </li>
                              </ul>
                            </div>
                            <div class="col-sm-10 mega-sub-topics hidden-sm hidden-xs">
                              <div class="row">
                                <div class="col-sm-8">
                                  <div class="vid-thumb">
                                    <h4>Paying for a Homebuyer Report?</h4>
                                    <a href="#">
                                      <div class="sub-box"><img class="img-thumbnail" src="images/lifestyle/lifestyle-slide-2.jpg" width="300" height="132" alt=""/> </div>
                                    </a> </div>
                                  </div>
                                  <div class="col-sm-8">
                                    <div class="sub-topic-thumb">
                                      <h4>Sugar-free chocolate recipes: dark chocolate</h4>
                                      <a href="#">
                                        <div class="sub-box"><img class="img-thumbnail" src="images/lifestyle/lifestyle-slide-1.jpg" width="300" height="132" alt=""/> </div>
                                      </a> </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </li>
                      </ul>
                    </li>
                    <li><a href="#">Tech</a></li> 
                    <li class="dropdown"> <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Entertainment
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#">HTML</a></li>
                        <li><a href="#">CSS</a></li>
                        <li><a href="#">Bootstrap</a></li> 
                      </ul>
                    </li>
                    <li><a href="">Community</a></li>
                  </ul>

                  <ul class="nav nav-x navbar-nav navbar-right">
                    <li><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                    <li>
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-h"></i></a>
                      <ul class="dropdown-menu text-capitalize mega-menu" role="menu">
                        <li>
                          <div class="row">
                            <div class="col-sm-16 pading30">
                              <div class="row">
                                <div class="col-xs-16 col-sm-16 col-md-4 col-lg-4">
                                  <h5 class="head-title">Media Sosial</h5>
                                  <ul class="sosialmedia">
                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-facebook">
                                        <i class="fa fa-facebook" style="color: #fff;"></i>
                                      </i> Facebook
                                    </li>

                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-twitter">
                                        <i class="fa fa-twitter" style="color: #fff;"></i>
                                      </i> Twitter
                                    </li>

                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-google-plus">
                                        <i class="fa fa-google-plus" style="color: #fff;"></i>
                                      </i> Google Plus
                                    </li>

                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-instagram">
                                        <i class="fa fa-instagram" style="color: #fff;"></i>
                                      </i> Instagram
                                    </li>

                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-facebook">
                                        <i class="fa fa-apps-store" style="color: #fff;"></i>
                                      </i> Apps Store
                                    </li>

                                    <li>
                                      <i class="btn btn-xs btn-social-icon btn-facebook">
                                        <i class="fa fa-google-play" style="color: #fff;"></i>
                                      </i> Google Play
                                    </li>

                                  </ul>
                                </div>
                                <div class="col-sm-12 mega-sub-topics hidden-sm hidden-xs">
                                 <h5 class="head-title">Jaringan</h5>
                                 <div class="row">
                                  <div class="col-sm-8">
                                    <ul class="jaringan">
                                      <li>Kabaroto</li>
                                      <li>BolaSkor</li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </li>

                  <li><a href="#search"><span class="fa fa-search"></span></a></li>
                </ul>

              </div><!-- /.navbar-collapse -->

            </div>


          </div>

        </nav>
        <!--nav end--> 

        <div id="search">
          <!--  -->
          <div class="container">
            <div class="row" style="margin-top: 5%;">
             <div id="custom-search-input">
              <div class="col-md-15 col-xs-14">
                <input type="text" class="search-query form-control" placeholder="Search" />
              </div>
              <div class="col-md-1 col-xs-1">
                <button type="button" class="close">×</button>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>


  </div>
  <!-- nav and search end--> 
</div>

