  <!-- Footer start -->
  <footer >
    <div class="top-sec">
      <div class="container ">
        <div class="row match-height-container boxer-footer">
          <div class="col-md-10 col-sm-16 col-xs-16">
            <div class="row">
              <div class="col-sm-16 ">
                <img src="images/general/status-logo.png">
                <br>
                <h5>Kantor Pusat</h5>
                <p>Jl. Bukit Gading Raya Komplek Gading Bukit Indah Blok TB-18 Jakarta Utara Kode Pos 14240 - Indonesia 
                <br>Telepon+62 21 245 205 71 Email: redaksi@merahputih.com</p>
                </div>

              </div>
            </div>

            <div class="col-sm-6 recent-posts hidden-x">
              <div style="background-color: #fff;border-radius: 5px;padding: 10px;">
                <h5 style="color: #ed1c24">TAHUKAH KAMU ?</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="btm-sec">
         <center>
          <div class="col-sm-16 col-md-16 mobile-show">
            <div class="panel-group" id="accordion" style="margin-bottom: 0px;">

              <div class="caret-up">
                <div>
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                      <span class="fa fa-bars" style="color: #fff;">
                      </span></a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse caret-up dropup">
                    <div class="panel-body">
                      <table class="table">
                        <tr>
                          <td><a href="#">TENTANG</a></td>
                        </tr>
                        <tr>
                          <td><a href="#">KONTAK KAMI</a></td>
                        </tr>
                        <tr>
                          <td><a href="#">REDAKSI</a></td>
                        </tr>
                        <tr>
                          <td><a href="#">KODE ETIK</a></td>
                        </tr>
                        <tr>
                          <td><a href="#">RSS</a></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </center>
          <div class="container">
            <div class="row boxer-footer">
              <div class="col-sm-16 center-x" style="padding: 0px;">
                <div class="row ">
                 <div class="col-md-8 col-sm-16 col-xs-16 copyrights">COPYRIGHT 2016 MERAHPUTIH.COM POWERED BY GROWINC TECHNOLOGY INDONESIA</div>

                 <div class="col-md-8 col-sm-16 col-xs-16 f-nav text-right hidden-x">
                  <ul class="list-inline ">
                    <li> <a href="#"> TENTANG </a> |</li>
                    <li> <a href="#"> KONTAK KAMI </a> |</li>
                    <li> <a href="#"> REDAKSI </a> |</li>
                    <li> <a href="#"> KODE ETIK </a> |</li>
                    <li> <a href="#"> RSS </a> </li>
                  </ul>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>


    </footer>
    <!-- Footer end -->

    <!-- menu mobile version -->
    <div class="navbar navbar-default  ">
      <div id="mySidenav" class="sidenav" role="navigation">

        <!-- Slide Menu -->
        <div id="menu" class="res-menu nav-mobile mobile-show" role="navigation">
          <div class="res-menu-holder">
            <!-- logo -->
            <div class="logo-holder">
              <a class="navbar-brand" href="javascript:void(0)" class="closebtn" onclick="closeNav()" 
              style="float:right; ">>&times;</a>   
            </div>
            <!-- logo -->
            <!-- menu -->
            <ul class="res-nav">
              <li>
                <a class="title-mobile" data-toggle="collapse" href="#list-1">News</a>
                <ul class="collapse sub-list" id="list-1">
                  <li><a href="">Version 1</a><i class="fa fa-genderless pull-right"></i></li>
                  <li><a href="">Version 2</a></li>
                  <li><a href="">version 3</a></li> 
                </ul>
              </li>
              <li>
                <a class="title-mobile" href="#">Hunian</a>
              </li>
              <li>
                <a class="title-mobile" data-toggle="collapse" href="#list-2">Lifestyle</a>
                <ul class="collapse sub-list" id="list-2">
                  <li><a href="index.html">Version 1</a><i class="fa fa-genderless pull-right"></i></li>
                  <li><a href="index-2.html">Version 2</a></li>
                  <li><a href="index-3.html">version 3</a></li> 
                </ul>
              </li>
              <li>
                <a class="title-mobile" href="#">Tech</a>
              </li>
              <li>
                <a class="title-mobile" data-toggle="collapse" href="#list-3">Entertainment</a>
                <ul class="collapse sub-list" id="list-3">
                  <li><a href="index.html">Version 1</a><i class="fa fa-genderless pull-right"></i></li>
                  <li><a href="index-2.html">Version 2</a></li>
                  <li><a href="index-3.html">version 3</a></li> 
                </ul>
              </li>
              <li>
                <a class="title-mobile" href="#">Community</a>
              </li>
            </ul>
            <!-- menu -->
            <!-- social icons -->
            <ul class="social-style-2">
              <li class="fb"><a href="#" class="fa fa-facebook"></a></li>
              <li class="tw"><a href="#" class="fa fa-twitter"></a></li>
              <li class="li"><a href="#" class="fa fa-linkedin"></a></li>
              <li class="sky"><a href="#" class="fa fa fa-skype"></a></li>
            </ul>
            <!-- social icons -->
            <!-- copyright -->
            <p>COPYRIGHT 2016 <br>MERAHPUTIH.COM POWERED BY <br>GROWINC TECH INDONESIA</p>
            <!-- copyright -->
          </div>
        </div>
        <!-- Slide Menu -->
      </div>
    </div>
    <!-- end mobile version -->



  </div>
  <!-- wrapper end --> 
  <!-- jQuery --> 
  <script src="js/jquery.min.js"></script> 
  <!--jQuery easing--> 
  <script src="js/jquery.easing.1.3.js"></script> 
  <!-- bootstrab js --> 
  <script src="js/bootstrap.js"></script> 
  <!--style switcher--> 
  <script src="js/style-switcher.js"></script> <!--wow animation--> 
  <!-- time and date --> 
  <!--news ticker--> 
  <script src="js/jquery.ticker.js"></script> 
  <!-- owl carousel --> 
  <script src="js/owl.carousel.js"></script> 
  <!-- magnific popup --> 
  <script type="text/javascript">
    $(document).ready(function() {   
      var sideslider = $('[data-toggle=collapse-side]');
      var sel = sideslider.attr('data-target');
      var sel2 = sideslider.attr('data-target-2');
      sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
      });
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function () {

      $('#memberModal').modal('show');  

    });
  </script>

  <script type="text/javascript">
    $(function () {
      $('a[href="#search"]').on('click', function(event) {
        event.preventDefault();
        $('#search').addClass('open');
        $('#search > form > input[type="search"]').focus();
      });

      $('#search, #search button.close').on('click keyup', function(event) {
        if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
          $(this).removeClass('open');
        }
      });


    //Do not include! This prevents the form from submitting for DEMO purposes only!
    $('form').submit(function(event) {
      event.preventDefault();
      return false;
    })
  });
</script>

<script type="text/javascript" src="css/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript">


  $("#owl-demo").owlCarousel({
    autoPlay : 3000,
    stopOnHover : true,
    navigation:false,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fade"
  });

  $("#owl-demo2").owlCarousel({
    autoPlay : 3000,
    stopOnHover : true,
    navigation:false,
    pagination:false,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    transitionStyle:"fade"
  });

</script>

<script type="text/javascript">
  function openNav() {
    document.getElementById("mySidenav").style.width = "270px";
    document.getElementById("main").style.marginLeft = "270px";

  }

  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
    
  }
</script>

</body>

</html>